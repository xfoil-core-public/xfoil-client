package dev.xfoil.services.ba;

import com.google.zxing.common.BitMatrix;
import dev.xfoil.models.BAResponse;
import dev.xfoil.models.BaActiveSessionResponse;
import dev.xfoil.models.ResponseStatus;

import java.util.List;

public interface BAServices {

    String scanOTPQrCode(String encryptedQrcode);

    BAResponse userLogin(String emailId, String password);

    ResponseStatus BACheckIn(String encryptedText, String username, String checkInTime, String role);

    ResponseStatus BACheckout(String sessionId, String username, String checkoutTime);

    BaActiveSessionResponse getAllActiveSessions(String username);

}
