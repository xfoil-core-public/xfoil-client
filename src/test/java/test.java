import com.google.gson.Gson;
import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeReader;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.xfoil.dev.service.grpc.CashDepositStatus;
import com.xfoil.dev.service.grpc.TransactionSummary;
import dev.xfoil.config.XfoilClient;
import dev.xfoil.models.*;
import dev.xfoil.services.ba.BAServices;
import dev.xfoil.services.ba.BAServicesImplementation;
import dev.xfoil.services.company.CompanyServices;
import dev.xfoil.services.company.CompanyServicesImplementation;
import dev.xfoil.services.rider.RiderServices;
import dev.xfoil.services.rider.RiderServicesImplementation;
import dev.xfoil.services.transaction.TransactionBalanceService;
import dev.xfoil.services.transaction.TransactionBalanceServiceImplementation;
import dev.xfoil.utils.Constants;
import dev.xfoil.utils.Util;
import io.netty.util.AbstractConstant;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class test {

    private static final Logger logger = Logger.getLogger("InfoLogging");
    private static RiderServices riderServices;
    private static TransactionBalanceService transactionBalanceService;
    private static CompanyServices companyServices;
    private static BAServices baServices;
    private static RiderResponse verifiedUser;
    private static DepositToAccountResponse depositToAccountResponse;
    private static ExecutorService executorService;
    private static TransactionReplayResponse replayTransactionResponse;


    public static void main(String[] args) throws Exception {

        //instances api-qa.getxfoil.com api-dev.getxfoil.com  100.78.125.59  100.100.169.55
        // (xfoil-api-key) 8b55ef4682ca459fadd084d68a05ef47
        XfoilClient.Init("100.78.125.59", "8b55ef4682ca459fadd084d68a05ef47"); //c6c5c9dcf2be48739338b895133ab2c0  9bc9b60c257a41e3955a9bb08d9e6da5
        // 94a504676e6941dba21b8009639be549 8b55ef4682ca459fadd084d68a05ef47
        riderServices = RiderServicesImplementation.getInstance();
        transactionBalanceService = TransactionBalanceServiceImplementation.getInstance();
        companyServices = CompanyServicesImplementation.getInstance();
        baServices = BAServicesImplementation.getInstance();

        executorService = Executors.newFixedThreadPool(1);

        //rider services

//        loopCheckingTest();
//        verifyRiderTest();
//        createRiderTest();
//        updateRiderTest();
//        activeInactiveRiderTest();
//        generateQRTest();
//        updateRiderFcmToken();

        //transaction balance services

//        getRiderBalanceForSingleDateTest();
//        getCompanyBalanceForSingleDateTest();
//        getCompanyBalanceForDateRangeTest();
//        getRiderTransactionForSingleDateTest();
//        getRiderTransactionIDsForSingleDateTest();
//        getCompanyTransactionForSingleDateTest();
//        getCompanyTransactionForDateRangeTest();
//        setRiderDueAmountTest();
//        getRiderDueAmountTest();
//        sentDepositToAccount();
//        replayTransactionTest();

        //company services
//        getCompanyRegion();
//        getCompanyLocationsByRegion();

        //BA services
//        generateOTPQRTest();
//        userLoginTest();
        BACheckInTest();
//        BACheckOutTest();
//        BAActiveSessionTest();


        //test
//        convertListToString();
    }


    private static void convertListToString() {
        ArrayList<BillInfoMap> list = dummySummaryList();
        String listString = list.stream().map(BillInfoMap::toString)
                .collect(Collectors.joining(", "));

        logger.info("qq convertListToString: " + listString);

        logger.info("qq convertStringToList: " + new ArrayList<>(Arrays.asList(listString)));
    }

    private static void verifyRiderTest() {
        RiderResponse response = riderServices.verifyRider("42545", "SITE", "Karachi");

//        RiderResponse response = riderServices.verifyRider("1009","Faisalabad"); //without location
//                riderServices.verifyRider("9999", "Retailo", "SITE");
        verifiedUser = response;
        if (response.getResponse_status().getSuccess()) {
            logger.info("qq verifyRider:- " + response.getFirstName() + " " + response.getLastName() + ", " + new Gson().toJson(response));
        } else {
            logger.info("qq verifyRider:- " + new Gson().toJson(response));
        }
    }

    private static void loopCheckingTest() {
        RiderAssociationResponse response = riderServices.getMachineAffiliation();
        logger.info("qq loopCheckingTest:- %s: " + response.getResponse_status().getSuccess());
    }

    private static void createRiderTest() {
        final RiderResponse[] response = {null};
            executorService.submit(()->{
                for(int i=0;i<10;i++){
                    response[0] =
                            riderServices.createRider(
                                    "Anees", "Ahmad Rasheed", "3520288978845",
                                    "03214525788", "2107", "Lahore"); //"FB-Area"

                    RiderResponse response1 =
                            riderServices.createRider(
                                    "Anees", "Ahmad Rasheed", "3520288978845",
                                    "03214525788", "2107", "Lahore");
                }

        });

        executorService.submit(() -> {
            RiderResponse response2 =
                    riderServices.createRider(
                            "Anees", "Ahmad Rasheed", "3520288978845",
                            "03214525788", "2107", "Lahore");
        });

        if (response[0] != null && response[0].getResponse_status().getSuccess()) {
            logger.info("qq createRiderTest: " + response[0].getFirstName());
        } else {
            logger.info("qq createRiderTest: " + new Gson().toJson(response[0]) /*response.getResponse_status().getSuccess()*/);
        }
    }

    private static void activeInactiveRiderTest() {
        ActiveInactiveRiderResponse response = riderServices.setRiderActiveInactive(1321158L, true);
        logger.info("qq activeInactiveRiderTest:- %s: " + response.getResponseStatus().getSuccess());
    }

    private static void updateRiderTest() {
        //1234567890122 //03333257378 //6453
        RiderResponse response = riderServices.updateRider(
                1321158L, "Saba", "Zafar",
                "1234567890122", "03333257378", "6453", "Karachi");
        logger.info("qq updateRiderTest:- %s: " + response.getFirstName());
    }

    private static void generateQRTest() {
        BitMatrix response = riderServices.generateQrCode("99991","Karachi");
        logger.info("qq generateQRTest:- %s: " + response.toString());
    }

    private static void updateRiderFcmToken(){
        ResponseStatus response = riderServices.updateRiderFcmToken(1321158L, "");
        if(response != null){
            logger.info("qq updateRiderFcmToken: " + response.getSuccess());
        }else{
            logger.info("qq updateRiderFcmToken: " + new Gson().toJson(response));
        }
    }

    //BA Services
    private static void generateOTPQRTest() {
        String response = baServices.scanOTPQrCode("fwvR2R0wnTcgIqbH7w1s8ZtuCALNe1mS0HdNYpcx3aw=");
        logger.info("qq generateOTPQRTest:- %s: " + response);
    }

    private static void userLoginTest() {
        BAResponse response = baServices.userLogin("moazzam.maqsood@xfoil.dev", "54321");
        if(response != null){
            logger.info("qq userLoginTest:- : " + new Gson().toJson(response));
        }else{
            logger.info("qq userLoginTest:- null: " + response.getResponse_status().getSuccess());
        }
    }

    //mhihUn8SZFa2m6P9DD/a3TjHgVUYMEFTGpTb8Zg2XgM=
    private static void BACheckInTest() {
        ResponseStatus response =
                baServices.BACheckIn("vDbnZthhzxHUuXJdnqza2y432Nkz0nIdUr8GTX2FoTk=", "moazzam.maqsood@io.co", "" + System.currentTimeMillis(),"admin");
        if(response != null){
            logger.info("qq BACheckOutTest:- : " + new Gson().toJson(response));
        }else{
            logger.info("qq BACheckOutTest:- null: " + response.getSuccess());
        }
    }

    private static void BACheckOutTest() {
        ResponseStatus response = baServices.BACheckout("5be31853a875478daf96aac77ac111da", "moazzam.maqsood@io.co", "" + System.currentTimeMillis());
        if(response != null){
            logger.info("qq BACheckOutTest:- : " + new Gson().toJson(response));
        }else{
            logger.info("qq BACheckOutTest:- null: " + response.getSuccess());
        }
    }

    private static void BAActiveSessionTest(){
        BaActiveSessionResponse response = baServices.getAllActiveSessions("moazzam.maqsood@io.co");
        if(response != null){
            logger.info("qq BAActiveSessionTest:- : " + new Gson().toJson(response));
        }else{
            logger.info("qq BAActiveSessionTest:- null: " + response.getResponseStatus().getSuccess());
        }
    }

    private static Properties getProperties(String fileName) {
        InputStream inputStream = null;
        Properties props = new Properties();
        try {
            inputStream = new FileInputStream(fileName);
            props.load(inputStream);
        } catch (Exception e) {
            logger.info("qq getProperties- " + e.getLocalizedMessage());
        }
        return props;
    }

    // transaction balance services
//
    private static void getRiderBalanceForSingleDateTest() {
        BalanceResponse balanceResponse = transactionBalanceService.getRiderBalanceForSingleDate(66L, "2021-10-05");
        logger.info("qq getRiderBalanceForSingleDateTest:- %s: " + balanceResponse.getBalance());
    }

    private static void getCompanyBalanceForSingleDateTest() {
        BalanceResponse balanceResponse = transactionBalanceService.getCompanyBalanceForSingleDate("Retailo", "2021-10-05");
        logger.info("qq getCompanyBalanceForSingleDateTest:- %s: " + balanceResponse.getBalance());
    }

    private static void getCompanyBalanceForDateRangeTest() {
        BalanceResponse balanceResponse = transactionBalanceService.getCompanyBalanceForDateRange("Retailo", "2021-10-04", "2021-10-05");
        logger.info("qq getCompanyBalanceForDateRangeTest:- %s: " + balanceResponse.getBalance());
    }

    private static void setRiderDueAmountTest() {
        BalanceResponse balanceResponse = transactionBalanceService.setRiderDueBalance(85099L, "Bazaar", "" + 5000, "" + 1500);
        if(balanceResponse != null){
            logger.info("qq setRiderDueAmountTest:- %s: " + balanceResponse.getBalance() + "--" + balanceResponse.getResponseStatus().getSuccess());
        }else{
            logger.info("qq setRiderDueAmountTest:- null" );
        }
    }

    private static void getRiderDueAmountTest() {
        BalanceResponse balanceResponse = transactionBalanceService.getRiderDueBalance(85099L);
        if(balanceResponse != null){
            logger.info("qq getRiderDueAmountTest:- %s: " + balanceResponse.getBalance() + "--" + balanceResponse.getResponseStatus().getSuccess());
        }else{
            logger.info("qq getRiderDueAmountTest:- null" );
        }
    }

    private static void getRiderTransactionForSingleDateTest() {
        TransactionResponse transactionResponse = transactionBalanceService.getRiderTransactionsForSingleDate(53L, "Retailo", "2021-10-05");
        logger.info("qq getRiderTransactionForSingleDateTest:- %s: " + transactionResponse.getResponseStatus().getSuccess() + "--" + (transactionResponse.getTransactionsList() != null && transactionResponse.getTransactionsList().size() > 0 ? transactionResponse.getTransactionsList().get(0).getTransactionId() : "list-null"));
        for (Transactions transaction : transactionResponse.getTransactionsList()) {
            logger.info("qq getRiderTransactionForSingleDateTest:- " + transaction.getTransactionId() + "::" + transaction.getTransactionAmount());
        }
    }

    private static void getRiderTransactionIDsForSingleDateTest() {
        List<String> transactionResponse = transactionBalanceService.getRiderTransactionsIdsForSingleDate(798L, "2022-04-04");
        logger.info("qq getRiderTransactionIDsForSingleDateTest:- %s: " + new Gson().toJson(transactionResponse));
        replayTransactionTest("71802559706296");
    }

    private static void getCompanyTransactionForSingleDateTest() {
        TransactionResponse transactionResponse = transactionBalanceService.getCompanyTransactionsForSingleDate("Retailo", "2021-10-05");
        logger.info("-----------------------------");
        for (Transactions transaction : transactionResponse.getTransactionsList()) {
            logger.info("qq getCompanyTransactionForSingleDateTest:- " + transaction.getTransactionId() + "::" + transaction.getTransactionAmount());
        }
    }

    private static void getCompanyTransactionForDateRangeTest() {
        TransactionResponse transactionResponse = transactionBalanceService.getCompanyTransactionsForDateRange("Retailo", "2021-10-04", "2021-10-05");
        logger.info("-----------------------------");
        for (Transactions transaction : transactionResponse.getTransactionsList()) {
            logger.info("qq getCompanyTransactionForDateRangeTest:- " + transaction.getTransactionId() + "::" + transaction.getTransactionAmount());
        }
    }

    private static void sentDepositToAccount(){
        //depositToAccount
            depositToAccountResponse = transactionBalanceService.depositCash(
                    "F9ItWE5vSTdTc8iEwRChUTODXr+/XJkqqlwAJEV5nZNknsBiWoowEjliJtU1VCLS6OrJA/pEJl59OKHoh3CxyFIvZJnk3ywt2zEEVGGyQrKsT1mCsitTpDNc1AzIYrlkWGpwon+0fOTT43z8BKFrM6RYPFB3mDMkZSSojKgdBl1hDzci6hMtmacrLREbPc7gkXVUhz0z9qCvS6b+hhWLhHCMC86E6FFM2ToQfKxvKmteYLF6/A/BmRf94hyESjIho+VWg+hpWfPneda6l6YTcSiCsbbxcfOr2B3+xhHhwfduntR0OFVzdq45JxSu6rgZobDWHru0XubeEXmk4DBPCw==");
            if(depositToAccountResponse != null){
                logger.info("qq depositCash: " + depositToAccountResponse.getCashDepositStatus().toString());
            }else{
                logger.info("qq depositCash: " + null);
            }
    }

    private static void replayTransactionTest(String transactionId){
        replayTransactionResponse = transactionBalanceService.replayTransaction(transactionId);
        if(replayTransactionResponse != null){
            logger.info("qq replayTransactionTest: " + replayTransactionResponse.getResponseStatus().getSuccess());
        }else{
            logger.info("qq replayTransactionTest: " + null);
        }
    }

    //company services

    private static void getCompanyRegion() {
        CompanyRegionLocationsResponse response = companyServices.getCompanyRegions();
        if (response != null && response.getResponse_status().getSuccess()) {
            logger.info("qq getCompanyRegion:- %s: " + new Gson().toJson(response.getRegionLocationsList()));
        } else {
            logger.info("qq getCompanyRegion:- %s: " + new Gson().toJson(response));
        }
    }

    private static void getCompanyLocationsByRegion() {
        CompanyRegionLocationsResponse response = companyServices.getLocationsByRegions("karachi");
        if (response != null && response.getResponse_status().getSuccess()) {
            logger.info("qq getCompanyLocationsByRegion:- %s: " + new Gson().toJson(response.getRegionLocationsList()));
        } else {
            logger.info("qq getCompanyLocationsByRegion:- %s: " + new Gson().toJson(response));
        }
    }

    private static ArrayList<BillInfoMap> dummySummaryList() {
        ArrayList<BillInfoMap> summaryArrayList = new ArrayList<>();
        summaryArrayList.add(new BillInfoMap("5000",1000000));
        summaryArrayList.add(new BillInfoMap("500",1000000));
        summaryArrayList.add(new BillInfoMap("50",1000000));
        return summaryArrayList;
    }

}
